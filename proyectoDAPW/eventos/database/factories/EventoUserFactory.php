<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Evento;
use App\Models\EventoUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventoUserFactory extends Factory
{
    protected $model = EventoUser::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        //  array('user_id', 'evento_id', 'fecha_venta', 'fecha_evento', 'precio', 'asiento', 'codigoqrcontrol')
        $totalUsuarios = User::all()->count();
        $totalEventos = Evento::all()->count();

        return [
            'user_id' => $this->faker->numberBetween(1, $totalUsuarios),
            'evento_id' => $this->faker->numberBetween(1, $totalEventos),
            'fecha_venta' => $this->faker->date(),
            'fecha_evento' => $this->faker->date(),
            'precio' => $this->faker->randomNumber(),
            'asiento' => $this->faker->buildingNumber(),
            'codigoqrcontrol' => $this->faker->ean13()
        ];
    }
}
