<?php

return [
    'Dashboard' => 'MikroVel - Mikrotik + Laravel',

    // General
    'gr_alta' => 'Dar de Alta',
    'gr_editar' => 'Editar',
    'gr_borrar' => 'Borrar',
    'gr_resetear' => 'Resetear',
    'gr_volver' => 'Volver',
    'gr_cerrar' => 'Cerrar',
    'gr_volver' => 'Volver',
    'gr_mikrotik' => 'Consulta del router Mikrotik.',
    'gr_mikrotik_info' => ' !ATENCIÓN!<br/><BR/>Se va a solicitar esta información al router Mikrotik,<br/>y se borrarán los datos actuales de la base de datos.',
    'gr_mikrotik_boton' => 'Obtener datos desde Mikrotik ??? BORRAR',
    'gr_mikrotik_boton_modal' => 'Obtener datos',
    'gr_mikrotik_iface_control' => 'Para modificar la Interfaz e IP de Administración de Mikrotik, tendrá que conectarse con Winbox al router y posteriormente editar el fichero .env de esta aplicación web.',
    'gr_mikrotik_gestion' => 'Interfaz de Administración del router Mikrotik',


    /// IPs
    'ips_identificacion' => 'Identificación de IP\'s - Mikrotik.',
    'ips_alta' => 'Alta de direcciones IP',
    'ips_explicacion' => 'Visualización y edición de las IP\'s que tenemos asignadas al Mikrotik con su máscara correspondiente. Por ejemplo: Router, DMZ, XUNTA, WAN, Gestion...',

    /// Speeds
    'speeds_identificacion' => 'Velocidades interfaces - Mikrotik.',
    'speeds_alta' => 'Alta de velocidades de las interfaces',
    'speeds_explicacion' => 'Velocidades por defecto preconfiguradas en Mikrotik - ! No modificar sin chequear en Mikrotik antes !',

    /// Interfaces
    'interfaces_identificacion' => 'Identificación de Interfaces del Mikrotik.',
    'interfaces_alta' => 'Alta de interfaces',
    'interfaces_explicacion' => 'Gestión de interfaces de Mikrokik. Ejemplo: INTRANET1, INTRANET2, DMZ1, DMZ2, XUNTA, WAN..',

    /// Bondings
    'bondings_identificacion' => 'Identificación de Bondings/Trunks.',
    'bondings_alta' => 'Alta de Bondings/Trunks',
    'bondings_explicacion' => 'Visualización y edición de los bondings/trunks en Mikrotik. Por ejemplo: INTRANET, DMZ,...',

    /// DNS servidores
    'dns_identificacion' => 'Gestión de servidores DNS.',
    'dns_alta' => 'Alta de DNS',
    'dns_explicacion' => 'Servidores DNS que usa Mikrotik.',

    /// DNS reenviadores
    'reenviadores_identificacion' => 'Gestión de Reenvíos DNS.',
    'reenviadores_alta' => 'Alta de Reenviador',
    'reenviadores_explicacion' => 'DNS para reeenvíos, por ejemplo a la xunta, a .local, etc.',

    /// Rutas
    'rutas_identificacion' => 'Identificación de Rutas.',
    'rutas_alta' => 'Alta de Rutas',
    'rutas_explicacion' => 'Tabla de enrutamiento.',

    /// Zonas
    'zonas_identificacion' => 'Gestión Zonas Firewall.',
    'zonas_alta' => 'Alta de Zona',
    'zonas_explicacion' => 'Zonas del Firewall.',

    /// DHCP Configs
    'dhcpconfigs_identificacion' => 'Gestión configuración DHCP.',
    'dhcpconfigs_alta' => 'Alta de configuración DHCP',
    'dhcpconfigs_explicacion' => 'Configuraciones generales de servidores DHCP.',

    /// DHCP Servidores
    'dhcpservidores_identificacion' => 'Gestión Servidores DHCP.',
    'dhcpservidores_alta' => 'Alta de servidor DHCP',
    'dhcpservidores_explicacion' => 'Gestión de Servidores (ámbitos) DHCP.',

    /// Equipos en DHCP leases reservas macs
    'equipos_identificacion' => 'Gestión Equipos en DHCP.',
    'equipos_alta' => 'Alta de equipo en DHCP',
    'equipos_explicacion' => 'Gestión de Equipos en el DHCP (reservas por MAC).',

    /// Puertos abiertos en Servidores DMZ
    'puertos_identificacion' => 'Gestión de Puertos disponibles.',
    'puertos_alta' => 'Alta de puertos',
    'puertos_explicacion' => 'Gestión de puertos usados por servidores Internos y de la DMZ. Los puertos externos nombrarlos como Puerto-ext e indicar puerto en IP pública e IP interna. Los puertos internos poner simplemente el nombre del puerto y el número de puerto en el servidor interno.',

    /// Servidores en la DMZ
    'servidores_identificacion' => 'Gestión Servidores en DMZ.',
    'servidores_alta' => 'Alta de servidor en DMZ',
    'servidores_explicacion' => 'Gestión de Servidores accesibles en la DMZ. Por ejemplo, si el servidor tiene acceso al puerto MySQL de cualquier servidor en la Intranet, marque en el segundo bloque el puerto permitido.',

    /// Servidores Internos
    'internos_identificacion' => 'Gestión Servidores Internos.',
    'internos_alta' => 'Alta de servidor Interno',
    'internos_explicacion' => 'Gestión de Servidores en la red interna del centro.',

    /// Firewall del Mikrotik
    'firewall_identificacion' => 'Gestión firewall del Mikrotik.',
    'firewall_explicacion' => 'Reglas Firewall del Mikrotik - consultas.',
    'firewall_filters' => 'Mostrar reglas Filter',
    'firewall_nats' => 'Mostrar reglas NAT',
    'firewall_mangles' => 'Mostrar reglas Mangle',
    'firewall_addresslists' => 'Mostrar Address Lists',
    'firewall_layer7protocols' => 'Mostrar Layer7 Protocols',
    'firewall_health' => 'Mostrar Health del Mikrotik',
    'firewall_borrartodo' => 'Borrar Todo del firewall',
    'firewall_setfirewall' => 'Regenerar el Firewall',
    'firewall_ports' => 'Mostrar puertos de servicio',
    'firewall_connections' => 'Mostrar conexiones',

    /// Mikrotik configuración
    'mikrotik_identificacion' => 'Gestión Datos Mikrotik.',
    'mikrotik_alta' => 'Alta de datos en Mikrotik',
    'mikrotik_explicacion' => 'Gestión de Mikrotik.',
    'mikrotik_health' => 'Mostrar Health de Mikrotik.',
    'mikrotik_aplicarcambios' => 'Aplicar cambios.',
];
