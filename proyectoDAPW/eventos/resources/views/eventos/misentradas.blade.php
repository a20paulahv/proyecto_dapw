@extends('plantillas.master')

@section('title')
Mis entradas adquiridas
@stop

@section('central')
<h2>Entradas compradas</h2>
<hr />
<table class="table">
    <thead>
        <tr>
            <th scope="col">Evento_id</th>
            <th scope="col">Fecha Venta</th>
            <th scope="col">Fecha del Evento</th>
            <th scope="col">Precio</th>
            <th scope="col">Asiento</th>
            <th scope="col">Codigo QR</th>
            <th scope="col">Edición</th>
        </tr>
    </thead>
    <tbody>
        @foreach($entradas as $entrada)
        <tr>
            <th scope="row">{{ $entrada->evento_id }}</th>
            <th scope="row">{{ \Carbon\Carbon::parse($entrada->fecha_venta)->format('d/m/Y') }}</th>
            <th scope="row">{{ \Carbon\Carbon::parse($entrada->fecha_evento)->format('d/m/Y') }}</th>
            <th scope="row">{{ $entrada->precio }}</th>
            <th scope="row">{{ $entrada->asiento }}</th>
            <th scope="row">{{ $entrada->codigoqrcontrol }}</th>
            <th scope="row"><a href="{{ route('eventos.infoentrada',$entrada) }}" class="btn btn-primary">Más detalle</a>
            </th>
        </tr>
        @endforeach
    </tbody>
</table>
@stop