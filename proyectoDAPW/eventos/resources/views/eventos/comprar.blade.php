@extends('plantillas.master')

@section('title')
Compra de Entradas
@stop

@section('central')
<h2>ALTA DE EVENTO</h2>
<form action="{{ route('eventos.comprarstore') }}" method="post">
    @include('eventos._formcomprar')
    <input type="reset" value="Limpiar" />
    <input type="submit" class="btn btn-primary" value="Comprar Entrada" />
</form>
@stop