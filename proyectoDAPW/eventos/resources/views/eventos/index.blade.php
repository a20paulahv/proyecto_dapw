@extends('plantillas.master')

@section('title')
Listado de Eventos
@stop

@section('central')
<h2>LISTADO DE EVENTOS DISPONIBLES</h2>
<hr />
@if(Session::has('mensaje'))
<div class="alert {{ Session::get('alert-class') }}">
    {{ Session::get('mensaje') }}
</div>
@endif
<hr />
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Titulo</th>
            <th scope="col">Telefono</th>
            <th scope="col">Direccion</th>
            <th scope="col">Cartel</th>
            <th scope="col">Coordenadas</th>
            <th scope="col">Fecha Inicio</th>
            <th scope="col">Fecha Fin</th>
            <th scope="col">Edición</th>
        </tr>
    </thead>
    <tbody>
        @foreach($eventos as $evento)
        <tr>
            <th scope="row">{{ $evento->id }}</th>
            <th scope="row">{{ $evento->titulo }}</th>
            <th scope="row">{{ $evento->telefono }}</th>
            <th scope="row">{{ $evento->direccion }}</th>
            <th scope="row">{{ $evento->cartel }}</th>
            <th scope="row"><a href="http://maps.google.com/?ie=UTF8&hq=&ll={{ $evento->latitud }},{{ $evento->longitud }}&z=13" target="_blank">Google Maps</a></th>

            <th scope="row">{{ $evento->fecha_inicio }}</th>
            <th scope="row">{{ $evento->fecha_fin }}</th>
            <th scope="row"><a href="{{ route('eventos.edit',$evento) }}" class="btn btn-primary">Editar</a>
                <form action="{{ route('eventos.destroy',$evento) }}" method="post">@csrf @method('DELETE') <input type="submit" class="btn btn-primary" value="Borrar" /></form> <a href="{{ route('eventos.show',$evento) }}" class="btn btn-primary">Info</a>
            </th>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $eventos->links() !!}

@stop