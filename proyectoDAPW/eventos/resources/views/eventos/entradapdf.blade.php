<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Entrada Eventalia</title>
</head>

<body>
    <center>
        SU ENTRADA EVENTALIA
        <H3>{{ $evento->titulo }}</H3>
    </center>
    <table>
        <tr valign="top" align="center">
            <td>
                <img src='{{ URL::asset("storage/$evento->cartel") }}' width="300" />
            </td>
            <td>
                <br /><br />
                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('svg')->size(150)->generate($entrada->codigoqrcontrol)) !!} " />
                <h4>Muestre este código QR<br /> a la entrada del espectáculo</h4>
            </td>
        </tr>
    </table>
    <hr />
    <center>
        Fecha de Compra: {{ \Carbon\Carbon::parse($entrada->fecha_venta)->format('d/m/Y') }} || Fecha Evento: {{ \Carbon\Carbon::parse($entrada->fecha_evento)->format('d/m/Y') }}<br /><br />
        Precio: {{ $entrada->precio }}<br />
        Su Asiento: {{ $entrada->asiento }}<br />
        <h3>Localizacion del Evento</h3>
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('svg')->size(150)->geo($evento->latitud, $evento->longitud)) !!} ">
    </center>
</body>

</html>