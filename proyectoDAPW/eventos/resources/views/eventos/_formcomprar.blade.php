@csrf
<div class="mb-3">
    <label for="titulo" class="form-label">Evento:</label>
    <select name="evento_id" class="form-control" id="evento_id">
        <option value="0">Seleccione un evento</option>
        @foreach ($eventos as $evento)
        <option value="{{ $evento->id }}">{{ $evento->titulo}}</option>
        @endforeach
    </select>
    @error('titulo')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="fecha_evento" class="form-label">Seleccione fecha:</label>
    <input type="date" class="form-control" id="fecha_evento" name="fecha_evento" value="{{ old('fecha_evento',$entrada->fecha_evento) }}">
    @error('fecha_evento')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>
<div class="mb-3">
    <label for="precio" class="form-label">Precio:</label>
    <input type="number" class="form-control" id="precio" name="precio" value="{{ old('precio',$entrada->precio) }}" maxlength="15" required>
    @error('precio')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>
<div class="mb-3">
    <label for="asiento" class="form-label">Asiento:</label>
    <input type="text" class="form-control" id="asiento" name="asiento" value="{{ old('asiento',$entrada->asiento) }}" maxlength="15">
    @error('asiento')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>