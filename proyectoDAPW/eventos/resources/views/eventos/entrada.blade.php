@extends('plantillas.master')

@section('title')
Detalles de la entrada
@stop

@section('central')
<h2>Entrada</h2>
<hr />
Evento: {{ $evento->titulo }}<br />
Foto: <img src='{{ URL::asset("storage/$evento->cartel") }}' width="320" /><br />
Fecha Venta: {{ \Carbon\Carbon::parse($entrada->fecha_venta)->format('d/m/Y') }}<br />
Fecha del Evento: {{ \Carbon\Carbon::parse($entrada->fecha_evento)->format('d/m/Y') }}<br />
Precio: {{ $entrada->precio }}<br />
Asiento: {{ $entrada->asiento }}<br />
<hr />
Codigo QR validación entrada: {!! QrCode::size(150)->generate($entrada->codigoqrcontrol) !!} <br />
<hr />
Código QR de Google Maps: {!! QrCode::size(150)->geo($evento->latitud, $evento->longitud) !!}
@stop