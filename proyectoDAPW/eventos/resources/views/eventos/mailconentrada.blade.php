<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>This is test mail</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container text-center m-1">
        <h1 class="lead mt-3">Enhorabuena por su nueva entrada.</h1>
        <p class="lead">En este correo va un fichero adjunto que contiene el PDF de su entrada.</p>
        <p class="lead">Gracias por comprar en Eventalia</p>
    </div>
</body>

</html>