<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    use HasFactory;
    // eventos (id, titulo, telefono, direccion, cartel, latitud, longitud, fecha_inicio, fecha_fin)

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = array('titulo', 'telefono', 'direccion', 'cartel', 'latitud', 'longitud', 'fecha_inicio', 'fecha_fin');

    public function usuarios()
    {
        return $this->belongsToMany(User::class)->withPivot(['fecha_venta', 'fecha_evento', 'precio', 'asiento', 'codigoqrcontrol']);
    }
}
