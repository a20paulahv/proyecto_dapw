#!/bin/bash
outfile="/backups/backup$(date +%y%m%d-%H%M).sql"
mysqldump -h "$(grep DB_HOST /var/www/html/.env | cut -d'=' -f2)" \
      -u "$(grep DB_USERNAME /var/www/html/.env | cut -d'=' -f2)" \
      -p"$(grep DB_PASSWORD /var/www/html/.env | cut -d'=' -f2)" \
      "$(grep DB_DATABASE /var/www/html/.env | cut -d'=' -f2)" > $outfile
echo "Saved to $outfile"