#!/bin/bash
cat $1 | mysql -h "$(grep DB_HOST /var/www/html/.env | cut -d'=' -f2)" \
      -u "$(grep DB_USERNAME /var/www/html/.env | cut -d'=' -f2)" \
      -p"$(grep DB_PASSWORD /var/www/html/.env | cut -d'=' -f2)" \
      "$(grep DB_DATABASE /var/www/html/.env | cut -d'=' -f2)"

if [ $? = 0 ]; then echo "Datos insertados con éxito";
else echo "Error"; fi