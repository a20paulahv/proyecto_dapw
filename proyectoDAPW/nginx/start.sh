#!/bin/sh
whoami
chown -R nginx:nginx /var/log/nginx/ /etc/nginx/certs/server.crt /etc/nginx/private/server.key
/docker-entrypoint.sh nginx -g "daemon off;"